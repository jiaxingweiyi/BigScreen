﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigScreen.Conmmon;

namespace BigScreen.Models
{
    /// <summary>
    /// 房屋概况
    /// </summary>
   public class CityProfile:PropertyChangedBase
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; SetPropertyChanged(); }
        }

        private double sold;

        public double Sold
        {
            get { return sold; }
            set { sold = value; SetPropertyChanged(); }
        }

        private double filing;

        public double Filing
        {
            get { return filing; }
            set { filing = value; SetPropertyChanged(); }
        }
      
        
    }
}
