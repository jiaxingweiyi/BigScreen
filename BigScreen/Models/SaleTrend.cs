﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigScreen.Models
{
    /// <summary>
    /// 销售趋势
    /// </summary>
    public class SaleTrend
    {
        /// <summary>
        /// 月份
        /// </summary>
        public string Month { get; set; }
        /// <summary>
        /// 回款
        /// </summary>
        public double BackMoney { get; set; }
        /// <summary>
        /// 签约
        /// </summary>
        public double Signed { get; set; }
        /// <summary>
        /// 认购
        /// </summary>
        public double Subscribe { get; set; }
    }
}
