﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BigScreen.Conmmon;

namespace BigScreen.Models
{
    public class TaskBar : PropertyChangedBase
    {
        /// <summary>
        /// 图标
        /// </summary>
        private string icon;

        public string Icon
        {
            get { return icon; }
            set { icon = value; SetPropertyChanged(); }
        }


        /// <summary>
        /// 内容
        /// </summary>
        private double val;

        public double Value
        {
            get { return val; }
            set { val = value; SetPropertyChanged(); }
        }


        /// <summary>
        /// 标题
        /// </summary>
        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; SetPropertyChanged(); }
        }



        /// <summary>
        /// 背景色
        /// </summary>

        private string backgroundColor;

        public string BackgroundColor
        {
            get { return backgroundColor; }
            set { backgroundColor = value; SetPropertyChanged(); }
        }

        /// <summary>
        /// 单位
        /// </summary>
        private string unit;

        public string Unit
        {
            get { return unit; }
            set { unit = value; }
        }


    }
}
