﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BigScreen.ViewModels;

namespace BigScreen.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new MainWindowViewModel();

            //最小化
            this.btnMin.Click += (s, e) =>
            {
                this.WindowState = WindowState.Minimized;
            };

            //最大化
            this.btnMax.Click += (s, e) =>
            {
                if (this.WindowState == WindowState.Maximized)
                {
                    this.WindowState = WindowState.Normal;
                    this.winBtnBar.Visibility = Visibility.Visible;
                }
                else
                {
                    this.WindowState = WindowState.Maximized;
                    //最大化隐藏按钮
                    this.winBtnBar.Visibility = Visibility.Hidden;
                }
            };

            //关闭
            this.btnClose.Click += (s, e) => { this.Close(); };

            //顶部导航栏可双击放大
            //不支持click事件的控件，判断是否双击了
            //this.headerBar.MouseDown += (s, e) =>
            //{
            //    //双击
            //    if (e.ClickCount == 2)
            //    {
            //        if (this.WindowState == WindowState.Maximized)
            //        {
            //            this.WindowState = WindowState.Normal;
            //            this.winBtnBar.Visibility = Visibility.Visible;
            //        }
            //        else
            //        {
            //            this.WindowState = WindowState.Maximized;
            //            //最大化隐藏按钮
            //            this.winBtnBar.Visibility = Visibility.Hidden;
            //        }
            //        e.Handled = true;
            //    }

            //};

            //拖拽移动
            //this.headerBar.MouseMove += (s, e) =>
            //{
            //    if (e.LeftButton == MouseButtonState.Pressed)
            //        this.DragMove();
            //};

            //ESC退出全屏
            this.KeyDown += (s, e) =>
            {
                if (e.Key == Key.Escape&&this.WindowState==WindowState.Maximized)
                {
                    this.WindowState = WindowState.Normal;
                    this.winBtnBar.Visibility = Visibility.Visible;
                }
            };
        }


    }
}
